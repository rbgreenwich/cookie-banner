
// Function to load resources, then execute f()
function load_resources(sources, f, index) {
	var script = document.createElement(sources[index]['tag'])

	for (var property in sources[index]) {
		if (property!="tag") {
			script.setAttribute(property, sources[index][property])
		}
	}

	script.onload = script.onreadystatechange = function() {
		if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
			if (index == sources.length-1) {
				if (typeof f == 'function') f()
				script.onload = script.onreadystatechange = null;
			} else if (index+1 <= sources.length-1) {
				load_resources(sources, f, index+1)
			}
		} else {
			console.warn("There has been an issue loading resource: "+sources[index])
			return;
		}
	};
	document.head.appendChild(script);
}


// Banner object, called for config and execution
function Banner(config_json) {
	// Move parameters from config_json into object
	if (config_json!=undefined) {
		if (config_json["path"]!=undefined) {
			this.path = config_json["path"]
		}
		if (config_json["GAID"]!=undefined) {
			this.GAID = config_json["GAID"]
		}
		if (config_json["domain"]!=undefined) {
			this.domain = config_json["domain"]
		}
		if (config_json["preferences_uri"]!=undefined) {
			this.preferences_uri = config_json["preferences_uri"]
		}
		if (config_json["preferences_selector"]!=undefined) {
			this.preferences_selector = config_json["preferences_selector"]
		}
		if (config_json["link_selector"]!=undefined) {
			this.link_selector = config_json["link_selector"]
		}
		if (config_json["link_html"]!=undefined) {
			this.link_html = config_json["link_html"]
		}
		if (config_json["heading"]!=undefined) {
			this.heading = config_json["heading"]
		}
		if (config_json["content"]!=undefined) {
			this.content_html = config_json["content"]
		}
		if (config_json["type"]!=undefined) {
			this.type = config_json["type"]
		}
	}

	var ts = Date.now()

	// Provide Banner().type for user to define cookie categories in
	if (this.type==undefined) {
		this.type = {}
	}

	// Load initial resources
	this.execute = (function() {
		// Default 'type' config - including scripts loaded after analytics acceptance
		if (this.GAID!=undefined) {
			if (this.type.analytics==undefined) {
				this.type.analytics = {}
			}
			if (this.type.analytics.content==undefined) {
				this.type.analytics.content = ""
			}
			if (this.type.analytics.scripts==undefined) {
				this.type.analytics.scripts = []
			}
			this.type.analytics.scripts.push('https://www.googletagmanager.com/gtag/js?id='+this.GAID)
			this.type.analytics.scripts.push(this.path+'/resources/js/google-analytics.js?gaid='+this.GAID+'&ts='+ts)
		}
		if (this.path==undefined || this.domain==undefined) {
			console.warn(
				"Vital variables for the cookie banner were not set. " +
				"Please make sure path, GAID and domain are all explicitly set."
			)
		}
		if (this.preferences_uri==undefined) {
			this.preferences_uri = []
		}
		if (this.preferences_selector==undefined) {
			this.preferences_selector = []
		}
		if (this.link_selector==undefined) {
			this.link_selector = ""
		}
		if (this.link_html==undefined) {
			this.link_html = ""
		}
		if (this.heading==undefined) {
			this.heading = "<h2>Cookie preferences</h2>"
		}
		if (this.content==undefined) {
			this.content = "<p>We use necessary cookies to make our site work. We'd also like to set analytics cookies that help us make improvements by measuring how you use the site. These will be set only if you accept.</p><p>For more detailed information about the cookies we use, see our <a href='https://royalgreenwich.gov.uk/cookies'>cookies page</a>.</p>"
		}
		if (this.tabindex==undefined) {
			this.tabindex = 1
		}

		// Resources to be loaded
		this.resources = [
			{
				'tag'  : 'link',
				'rel'  : 'stylesheet',
				'type' : 'text/css',
				'href' : this.path+'/resources/css/imports.css?ts='+ts
			},
			{
				'tag'                : 'script',
				'data-heading-html'  : this.heading,
				'data-content-html'  : this.content,
				'data-domain'        : this.domain,
				'data-pref-uris'     : JSON.stringify(this.preferences_uri),
				'data-pref-selector' : this.preferences_selector,
				'data-link-selector' : this.link_selector,
				'data-link-html'     : this.link_html,
				'data-cookie-types'  : JSON.stringify(this.type),
				'data-tabindex'      : this.tabindex,
				'src'                : this.path+'/resources/js/cookie-banner.js?ts='+ts
			}
		]

		var cb = this

		load_resources(this.resources, function() {
			// Load resources for accepted cookie types
			if (cb.type!=undefined) {
				Object.entries(cb.type).forEach(function(v, i, c) {
					var type_resources = []
					if (v[1].scripts!=undefined) {
						v[1].scripts.forEach(function(src) {
							new_script = {}
							new_script.tag = "script"
							new_script.src = src
							type_resources.push(new_script)
						})
						if (getAcceptedTypes().includes(v[0])) {
							load_resources(type_resources, false, 0)
						}
					}
				})
			}
		}, 0)
	})
}

