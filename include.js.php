<?php

header("Content-Type: application/javascript");
header("Access-Control-Allow-Origin: *");

// Read in include.min.js and spit out with edited CORS headers
echo file_get_contents(__DIR__."/include.js");
