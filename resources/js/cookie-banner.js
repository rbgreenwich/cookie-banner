
var domain = document.currentScript.getAttribute('data-domain')
var preferences_uri = JSON.parse(document.currentScript.getAttribute('data-pref-uris'))
var preferences_selector = document.currentScript.getAttribute('data-pref-selector')
var link_selector = document.currentScript.getAttribute('data-link-selector')
var link_html = document.currentScript.getAttribute('data-link-html')
var heading_html = document.currentScript.getAttribute('data-heading-html')
var content_html = document.currentScript.getAttribute('data-content-html')
var cookie_types = Object.entries(JSON.parse(document.currentScript.getAttribute('data-cookie-types')))
var tabindex = document.currentScript.getAttribute('data-tabindex')

var cookie_type_names = []
cookie_types.forEach(function(v, i) {
	cookie_type_names.push(v[0])
})

updateConsentCookieTypes(true)

/**
 * Wait for an element to exist, then return it
 */
function wait_for_element(selector) {
	var element = document.querySelector(selector)
	if (element!=null) {
		return element
	} else {
		return setTimeout(function() {
			console.warn("Element '"+selector+"' was missing. Retrying...")
			return wait_for_element(selector)
		}, 1000)
	}
}

var cookies_allowed = true
setCookie("CookieBannerTest","true")
if (getCookie("CookieBannerTest")!="true") {
	console.warn("Cookies blocked. Banner will not be raised.")
	cookies_allowed = false
}
rmCookie("CookieBannerTest")

// Display cookie banner
if (cookies_allowed==true) {
	if (link_selector!="") {
		wait_for_element(link_selector).innerHTML+= link_html
		document.querySelector("#cookie-banner-button").onclick = function() {load_banner()}
	}
	if (getCookie("rbg_essential_cookies")!="true" && getCookie("cookie-agreed")=="") {
		load_banner()
	}
}

function load_banner() {
	var cookie_banner = document.createElement("div")
	cookie_banner.id = "cookie-banner"
	cookie_banner.classList.add("cookie-banner")

	var cookie_content_div = document.createElement("div")
	cookie_content_div.id = "cookie-content"

	let cookie_content = heading_html
	cookie_content+= content_html
	cookie_content+= "<div class='cookies_button_container'>"
	cookie_content+= "<button id='cookies_accept_all' class='button'>Accept all cookies</button>"
	cookie_content+= "<button id='cookies_accept_necessary' class='button'>Accept necessary cookies only</button>"
	cookie_content+= "<button id='cookies_config' class='button'>Change cookie settings</button>"
	cookie_content+= "</div>"
	cookie_content_div.innerHTML = cookie_content
	wait_for_element("body").appendChild(cookie_banner)
	document.querySelector("#cookie-banner").appendChild(cookie_content_div)

	document.querySelector("div#cookie-banner button#cookies_accept_all").onclick = function() {
		setAcceptanceCookies(cookie_type_names)
		document.querySelector("div#cookie-banner").remove()
	}
	document.querySelector("div#cookie-banner button#cookies_accept_necessary").onclick = function() {
		setAcceptanceCookies(["essential"])
		document.querySelector("div#cookie-banner").remove()
	}
	document.querySelector("div#cookie-banner button#cookies_config").onclick = function() {
		document.querySelector("div#cookie-banner").remove()
		overlay = document.createElement("div")
		overlay.setAttribute("id","cookie-overlay")
		dialog = document.createElement("div")
		dialog.setAttribute("id","cookie-dialog")
		overlay.appendChild(dialog)
		settings_content = "<h1>Cookie preferences</h1>"
		settings_content+= "<form id='cookie-preferences'>"
		settings_content+= "<fieldset class='disabled'>"
		settings_content+= "<legend>Strictly necessary cookies (required)</legend>"
		settings_content+= "<p>Necessary cookies enable core functionality such as security, network management, and accessibility. You may disable these by changing your browser settings, but this may affect how the website functions.</p>"
		settings_content+= "<fieldset class='radio'>"
		settings_content+= "<label>On<input type='radio' name='necessary-cookies' value='On' disabled checked='checked' /></label>" 
		settings_content+= "<label>Off<input type='radio' name='necessary-cookies' value='Off' disabled/></label>"
		settings_content+= "</fieldset>"
		settings_content+= "</fieldset>"
		cookie_types.forEach(function(v, i) {
			// Look at existing cookies for checkbox presets
			preselect_on = ""
			preselect_off = ""
			let accepted_types = getAcceptedTypes()
			if (v[1].force=="on" || (v[1].force==undefined && accepted_types.includes(v[0]) )) {
				preselect_on = "checked='checked'"
				preselect_off = ""
			} else if (v[1].force=="off" || (v[1].force==undefined && !accepted_types.includes(v[0]) )) {
				preselect_on = ""
				preselect_off = "checked='checked'"
			}
			let disabled = (v[1].force!=undefined?"disabled=disabled":"")
			let fieldset_class = (v[1].force!=undefined?"disabled":"")
			settings_content+= "<fieldset class='"+fieldset_class+"'>"
			settings_content+= "<legend>"+v[0].replace(/^\w/, (c) => c.toUpperCase())+" cookies</legend>"
			settings_content+= (v[1].content!=undefined?v[1].content:"")
			settings_content+= "<fieldset class='radio'>"
			settings_content+= "<label>On<input type='radio' id='"+v[0]+"-acceptance' name='"+v[0]+"-cookies' value='On' "+preselect_on+" "+disabled+"/></label>"
			settings_content+= "<label>Off<input type='radio' name='"+v[0]+"-cookies' value='Off' "+preselect_off+" "+disabled+"/></label>"
			settings_content+= "</fieldset>"
			settings_content+= "</fieldset>"
		})
		settings_content+= "<button type='submit' id='apply-cookies' class='button'>Save & exit</button>"
		settings_content+= "</form>"
		dialog.innerHTML = settings_content
		document.querySelector("body").appendChild(overlay)
		document.body.style.overflow = "hidden"
		dialog.querySelector("button#apply-cookies").onclick = (function() {
			var data = new FormData(document.forms['cookie-preferences'])
			cookie_types.forEach(function(f) {
				var val = data.get(f[0]+"-cookies")
				if (val==undefined) return false;
				types = [];
				if (val=="On") {
					types.push(f[0])
				}
				var name = f[0]
			})
			setAcceptanceCookies(types)
			overlay.remove()
			document.body.style.overflow = "auto"
		})
	}
	document.querySelector("#cookie-banner").focus()
}

/**
 * Retrieves a list of types that have been accepted by the user
 * @returns {array}
 */
function getAcceptedTypes() {
	const decoded = decodeURIComponent(document.cookie)
	if (decoded=="") return []

	const cookies = decoded.split(';')

	let consent_cookies = {}
	cookies.map(function(value, index, array) {
		const [cname, cval] = value.split('=');
		let cookie_obj = {}
		cookie_obj[cname.trim()] = cval.replace(/_cookies/g,"").trim()
		consent_cookies = {...consent_cookies, ...cookie_obj}
	})

	if (typeof consent_cookies['cookie-agreed'] == 'undefined') return []

	if (consent_cookies['cookie-agreed']==0) {
		// Essential only
		return ['strictly_necessary_cookies']
	} else {
		return consent_cookies['cookie-agreed-categories']
	}

	return []
}

/**
 * Creates cookies compatible with the banner used by the EU Cookie Compliance Drupal module
 * @param {array} types - list of cookie types to accept, e.g. ["essential","analytics"]
 * Creates:
 * cookie-agreed: contains an int representing the consent status (0 for essential-only, 2 for additional)
 * cookie-agreed-categories: contains an array of accepted cookies
 * ["strictly_necessary_cookies","functional_cookies","analytics_cookies"]
 * or:
 * [%22strictly_necessary_cookies%22%2C%22functional_cookies%22%2C%22analytics_cookies%22]
 */
function setAcceptanceCookies(types) {
	// Remove essential cookies from the list before processing
	['essential','necessary','strictly_necessary'].forEach(function(v){
		let search = types.indexOf(v);
		if (search > -1) {
			types.splice(search, 1);
		}
	})
	if (types.length==0) {
		// Essential only
		setCookie("cookie-agreed","0")
		rmCookie("cookie-agreed-categories")
		rmCookie("cookie-agreed-version")
	} else {
		// Additional cookies
		setCookie("cookie-agreed","2")

		let types_cookies = types.map(function(value, index, array) {
			return value+"_cookies"
		})
		setCookie("cookie-agreed-categories",JSON.stringify(["strictly_necessary_cookies", ...types_cookies]))

		// Expected by Drupal, no relation to the version of this code
		setCookie("cookie-agreed-version","1.0.0")
	}
}

// Create or refresh a cookie
function setCookie(name, value) {
	let cookie_domain = domain
	if (domain!="") {
		cookie_domain = "; domain="+domain
	}

	let expiry_date = new Date()
	expiry_date.setDate(expiry_date.getDate() + 100)
	expiry_date = expiry_date.toUTCString();

	document.cookie = name+"="+value+"; expires="+expiry_date+"; path=/; SameSite=Lax"+cookie_domain
}

// Remove a cookie
function rmCookie(name) {
	let cookie_domain = domain
	if (domain!="") {
		cookie_domain = "; domain="+domain
	}

	expiry_date = "Thursday, 1 January 1970 00:00:00"
	document.cookie = name+"=rm; expires="+expiry_date+"; path=/"+cookie_domain
}

/**
 * Update the old style of consent cookie to the new style, which
 *   follows the EU Cookie Compliance Drupal module
 * @param rm - if true, remove the type cookies after reading
 * @returns an array of cookie types previously accepted
 */
function updateConsentCookieTypes(rm=false) {
	let cookies = decodeURIComponent(document.cookie).split(';')
	let types = []
	for (let i=0; i<cookies.length; i++) {
		let [name, value] = cookies[i].trim().split('=')
		if (name.indexOf("rbg_")==-1 || value!="true") {
			// Only retrieve rbg cookies that are true
			continue 
		}
		let raw = name.substring(4).replace(/_cookies/g,"")
		if (raw=="essential") {
			raw = "strictly_necessary"
		}
		types.push(raw)
		if (rm) {
			rmCookie(name)
		}
	}
	if (types.length>0) {
		return setAcceptanceCookies(types)
	} else {
		return types
	}
}

function getCookie(cname) {
	var name = cname + "="
	var decodedCookie = decodeURIComponent(document.cookie)
	var ca = decodedCookie.split(';')
	for (var i = 0; i <ca.length; i++) {
		var c = ca[i]
		while (c.charAt(0) == ' ') {
			c = c.substring(1)
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length)
		}
	}
	return ""
}

